public class Main {
    public static void main(String[] args) {

        /*
        InvalidWeaponException
        InvalidArmorException
        StringBuilder
         */

        Mage mage = new Mage("Sondre the Almighty");
        mage.setEquipment(new Weapon("Staff", 1, 5,2, WeaponType.STAFF));
        mage.setEquipment(new Armor("Cloth", 1, Slots.BODY,ArmorType.CLOTH, new PrimaryAttribute(3,3,3,3)));
        mage.setEquipment(new Armor("Cloth", 1, Slots.BODY,ArmorType.CLOTH, new PrimaryAttribute(1,1,1,1)));

        mage.getStats();
    }
}
