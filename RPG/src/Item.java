public abstract class Item {

    private String itemName;
    private Slots slot;
    private int levelRequirement;

    public Item(String itemName, int levelRequirement, Slots slot){
        this.itemName = itemName;
        this.levelRequirement = levelRequirement;
        this.slot = slot;
    }

    //return boolean, checks if character has sufficient level to use item.
    public boolean levelCheck(Character character){ return character.getLevel() >= levelRequirement; }

    //return string itemName
    public String getItemName(){ return itemName; }

    //return enum Slots
    public Slots getSlot() { return slot; }

    //return int item level requirement
    public int getLevelRequirements(){ return levelRequirement; }

    //For testing purposes. Make additional check for item type in subclasses
    public boolean equals(Item item){
        return (this.itemName.equals(item.getItemName()) &&
                this.levelRequirement == item.getLevelRequirements() &&
                this.slot == item.getSlot()) &&
                ((this instanceof Weapon && item instanceof Weapon) || (this instanceof Armor && item instanceof Armor));
    }
}

