//Armor is a subclass of item. Handle the behavior of items of type armor.
public class Armor extends Item{

    private ArmorType armorType;
    private PrimaryAttribute armorAttributes;

    //Constructor
    public Armor(String name, int levelRequirement, Slots slot, ArmorType armorType, PrimaryAttribute armorAttributes){
        super(name, levelRequirement, slot);
        this.armorType = armorType;
        this.armorAttributes = armorAttributes;
    }

    //return enum ArmorType
    public ArmorType getArmorType(){ return armorType; }

    //return attributes of armor
    public PrimaryAttribute getArmorAttributes(){
        return armorAttributes;
    }

    //For testing purposes.
    public boolean equals(Armor armor){ return super.equals(armor) && (this.armorType == armor.getArmorType()); }

}
