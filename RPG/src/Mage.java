
public class Mage extends Character {

    public Mage(String name) { super(name, "mage", new PrimaryAttribute(1,1,8,5)); }

    public void levelUp() {
        level++;
        primaryAttribute.addAttribute(new PrimaryAttribute(1,1,5,3));
    }

    public String getClassName() { return "Mage";}

    public boolean canUseWeapon(Weapon weapon){
        return ((weapon.getWeaponType() == WeaponType.STAFF ||
                weapon.getWeaponType() == WeaponType.WAND) &&
                weapon.levelCheck(this));
    }

    public boolean canUseArmor(Armor armor){
        return ( (armor.getArmorType() == ArmorType.CLOTH) && armor.levelCheck(this) ); }

    @Override
    public float getDamage(){ return  (1 + ((float)(totalAttributes.getIntelligence())/100)) * this.getCharacterWeaponDPS(); }
}
