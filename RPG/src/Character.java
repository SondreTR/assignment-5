import java.util.HashMap;
import java.util.Map;

public abstract class Character {

    private final String name;
    private final String className;

    protected int level = 1;

    HashMap<Slots, Item> equipment;

    protected PrimaryAttribute primaryAttribute; // baseAttribute
    protected PrimaryAttribute totalAttributes;

    /*
    Make instance of a character, having a name, a class, a set of base attributes, and a set of total attributes (base attributes + item attributes)
    and a set of slots for equipment.
     */
    public Character(String name, String className, PrimaryAttribute primaryAttribute) {

        this.name = name;
        this.className = className;
        this.primaryAttribute = primaryAttribute;
        this.totalAttributes = primaryAttribute;

        equipment = new HashMap<>();
        equipment.put(Slots.WEAPON, null);
        equipment.put(Slots.HEAD, null);
        equipment.put(Slots.LEGS, null);
        equipment.put(Slots.BODY, null);
    }

    //return string character name
    public String getName() { return this.name; }

    //return int character level
    public int getLevel() { return level; }

    //abstract method for leveling up character
    public abstract void levelUp();

    //return boolean, check if character support use of given Armor
    public abstract boolean canUseArmor(Armor armor);

    //return boolean, check if character support use of given Weapon
    public abstract boolean canUseWeapon(Weapon weapon);

    //return boolean, check if character support use of given item
    public boolean canUseItem(Item item){
        if(item instanceof Weapon) return canUseWeapon((Weapon) item);
        else return canUseArmor((Armor) item);
    }

    //return Item in given slot
    public Item getEquipment(Slots slot){
        if(equipment.get(slot) == null) throw new NullPointerException("No equipment in slot");
        return equipment.get(slot);
    }

    //set given item to its slot
    public void setEquipment(Item item) {
        if(!this.canUseItem(item)){
            System.out.println(className + " does not support this item class.");
            return;
        }
        if(!item.levelCheck(this)){
            System.out.println(name + " doesn't have the require level to wear that");
            return;
        }
        if(equipment.containsKey(item.getSlot())){
            equipment.put(item.getSlot(), item);
            addEquipmentAttributes();
        }
        else throw new IllegalArgumentException("No such slot!");
    }

    //update totalAttributes based on the baseAttributes and the worn equipment
    private void addEquipmentAttributes(){
        totalAttributes = new PrimaryAttribute(primaryAttribute.getStrength(), primaryAttribute.getDexterity(), primaryAttribute.getIntelligence(), primaryAttribute.getVitality());
        for(Map.Entry<Slots, Item> entry : equipment.entrySet()){
            if(entry.getValue() instanceof Weapon || entry.getValue() == null) continue;
            totalAttributes.addAttribute(((Armor) entry.getValue()).getArmorAttributes());
        }
    }

    //show worn equipment
    public void seeEquipment(){
        for(Map.Entry<Slots, Item> entry : equipment.entrySet()) {
            if(entry.getValue() != null) System.out.println(entry.getKey().toString() + ": " + entry.getValue().getItemName());
            else System.out.println(entry.getKey().toString() + ": ");
        }
    }

    //return int character damage per second, based on characters main attributes and worn equipment
    public int getCharacterWeaponDPS(){
        if (equipment.get(Slots.WEAPON) == null) return 1;
        return ((Weapon) equipment.get(Slots.WEAPON)).getDPS();
    }

    //print character stats
    public void getStats(){
        System.out.println("Name: " + name);
        System.out.println("Class: " + className);
        System.out.println("Level: " + level);
        System.out.println("Health: " + 10*totalAttributes.getVitality());
        System.out.println("DPS: " + getCharacterWeaponDPS() + "\n");

        totalAttributes.getStats();
        System.out.println();
        seeEquipment();
    }

    /*
    return float character damage. since rouge and ranger are based on the same main attribute for damage, i've made their
    calculation for damage the super method, and overridden the methode for mage and warrior. This is not the best solution with more
    classes but is sufficient here.
     */
    public float getDamage(){ return (1 + ((float)(totalAttributes.getDexterity())/100)) * this.getCharacterWeaponDPS();}

    //For testing purposes.
    public PrimaryAttribute getPrimaryAttribute() { return primaryAttribute; }
}
