public class PrimaryAttribute {

    private int strength;
    private int dexterity;
    private int intelligence;
    private int vitality;

    public PrimaryAttribute(int strength, int dexterity, int intelligence, int vitality){
            this.strength = strength;
            this.dexterity = dexterity;
            this.intelligence = intelligence;
            this.vitality = vitality;
    }

    //add given attribute to this attribute
    public void addAttribute(PrimaryAttribute attribute) {
        this.strength += attribute.strength;
        this.dexterity += attribute.dexterity;
        this.intelligence += attribute.intelligence;
        this.vitality += attribute.vitality;
    }

    //print stats
    public void getStats(){
        System.out.println("Strength: " + strength);
        System.out.println("Intelligence: " + dexterity);
        System.out.println("Dexterity: " + intelligence);
        System.out.println("Vitality: " + vitality);
    }

    public int getStrength() { return strength; }
    public int getDexterity() { return dexterity; }
    public int getIntelligence() { return intelligence; }
    public int getVitality() { return vitality; }

    //For testing purposes.
    public boolean equals(PrimaryAttribute p){
        return (this.strength == p.getStrength() && this.dexterity == p.getDexterity() &&
                this.intelligence == p.getIntelligence() && this.vitality == p.getVitality());
    }
}
