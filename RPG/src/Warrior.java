
public class Warrior extends Character {

    //Constructor
    public Warrior(String name) { super(name, "Warrior", new PrimaryAttribute(5,2,1,10)); }

    public void levelUp() {
        level++;
        primaryAttribute.addAttribute(new PrimaryAttribute(3,2,1,5));
    }

    public String getClassName() { return "Warrior";}

    public boolean canUseWeapon(Weapon weapon){
        return ((weapon.getWeaponType() == WeaponType.AXE || weapon.getWeaponType() == WeaponType.HAMMER ||
                weapon.getWeaponType() == WeaponType.SWORD ) && weapon.levelCheck(this));
    }

    public boolean canUseArmor(Armor armor){ return ((armor.getArmorType() == ArmorType.MAIL || armor.getArmorType() == ArmorType.PLATE) && armor.levelCheck(this)); }

    @Override
    public float getDamage(){ return(1 + ((float)(totalAttributes.getStrength())/100)) * this.getCharacterWeaponDPS(); }

}

