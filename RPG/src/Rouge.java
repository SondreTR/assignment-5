
public class Rouge extends Character {

    public Rouge(String name) { super(name, "Rouge", new PrimaryAttribute(2,6,1,8)); }

    public void levelUp() {
        level++;
        primaryAttribute.addAttribute(new PrimaryAttribute(1,4,1,3));
    }

    public String getClassName() { return "Rouge";}

    public boolean canUseWeapon(Weapon weapon){
        return ((weapon.getWeaponType() == WeaponType.DAGGER ||
                weapon.getWeaponType() == WeaponType.SWORD) &&
                weapon.levelCheck(this)); }

    public boolean canUseArmor(Armor armor){
        return ((armor.getArmorType() == ArmorType.LEATHER) ||
                (armor.getArmorType() == ArmorType.MAIL) &&
                        armor.levelCheck(this)); }
}
