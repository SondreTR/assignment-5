
public class Ranger extends Character {

    public Ranger(String name) { super(name, "ranger", new PrimaryAttribute(1,7,1,8)); }

    public void levelUp() {
        level++;
        primaryAttribute.addAttribute(new PrimaryAttribute(1,5,1,2));
    }

    public String getClassName() { return "Ranger";}

    public boolean canUseWeapon(Weapon weapon){
        return ((weapon.getWeaponType() == WeaponType.BOW) && weapon.levelCheck(this));
    }

    public boolean canUseArmor(Armor armor){
        return (((armor.getArmorType() == ArmorType.LEATHER) || (armor.getArmorType() == ArmorType.MAIL)) &&
                armor.levelCheck(this));
    }
}

