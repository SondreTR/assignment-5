
/*
Weapon is a subclass of item. Handle the behavior of items of type weapon.
 */
public class Weapon extends Item{

    private int attackSpeed;
    private final int damage;

    private WeaponType weaponType;

    //Constructor
    public Weapon(String name, int levelRequirement, int damage, int attackSpeed, WeaponType weaponType){
        super(name, levelRequirement, Slots.WEAPON);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.weaponType = weaponType;
    }

    //returns Damage Per Second of weapon
    public int getDPS(){
        return damage * attackSpeed;
    }

    //return Enum WeaponType of weapon
    public WeaponType getWeaponType(){ return weaponType; }

    //For testing purposes.
    public boolean equals(Weapon weapon){ return super.equals(weapon) && (this.weaponType == weapon.getWeaponType()); }
}

