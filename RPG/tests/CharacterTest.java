import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CharacterTest extends VariablesForTesting {

    Mage testMage = new Mage("TestName");

    @Test
    void getName_shouldReturnStringName(){
        String actual = testMage.getName();
        String expected = "TestName";

        assertEquals(expected, actual);
    }

    @Test
    void getLevel_shouldReturnLevelOne(){
        int actual = testMage.getLevel();
        int expected = 1;

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_shouldReturnLevelTwoWhenLeveledUpOnce(){
        testMage.levelUp();

        int actual = testMage.getLevel();
        int expected = 2;

        assertEquals(expected, actual);
    }

    @Test
    void setEquipment_shouldSetEquipment(){
        testMage.setEquipment(cloth);

        Item actual = testMage.getEquipment(Slots.BODY);
        Item expected = new Armor("Cloth", 1, Slots.BODY, ArmorType.CLOTH, new PrimaryAttribute(1,1,1,1));

        assertTrue(expected.equals(actual));
    }

    @Test
    void canUseItem_shouldReturnTrue_whenGivenWeaponThatClassCanUse(){
        boolean actual = testMage.canUseItem(wand);
        assertTrue(actual);
    }

    @Test
    void canUseItem_shouldReturnFalse_whenGivenWeaponThatClassCannotUse(){
        boolean actual = testMage.canUseItem(bow);
        assertFalse(actual);
    }

    @Test
    void canUseItem_shouldReturnTrue_whenGivenArmorThatClassCanUse(){
        boolean actual = testMage.canUseItem(cloth);
        assertTrue(actual);
    }

    @Test
    void canUseItem_shouldReturnFalse_whenGivenArmorThatClassCannotUse(){
        boolean actual = testMage.canUseItem(mail);
        assertFalse(actual);
    }

    @Test
    void getEquipment_shouldReturnWeapon_whenWeaponIsEquipped(){
        testMage.setEquipment(staff);

        Weapon actual = (Weapon) testMage.getEquipment(Slots.WEAPON);
        Weapon expected = new Weapon("TestWeapon", 1,1,1, WeaponType.STAFF);

        assertTrue(actual.equals(expected));
    }

    @Test
    void getEquipment_shouldReturnArmor_whenArmorIsEquipped(){
        testMage.setEquipment(cloth);

        Armor actual = (Armor) testMage.getEquipment(Slots.BODY);
        Armor expected = new Armor("Cloth", 1, Slots.BODY, ArmorType.CLOTH, new PrimaryAttribute(1,1,1,1));

        assertTrue(actual.equals(expected));
    }

    @Test
    void getCharacterWeaponDPS_shouldReturnOne_whenNoWeaponIsEquipped(){
        int actual = testMage.getCharacterWeaponDPS();
        int expected = 1;

        assertEquals(expected, actual);
    }

    @Test
    void getCharacterWeaponDPS_shouldReturnDamage_whenWeaponIsEquipped(){
        testMage.setEquipment(staff);

        int actual = testMage.getCharacterWeaponDPS();
        int expected = 2*3;

        assertEquals(expected, actual);
    }

    @Test
    void getPrimaryAttribute_shouldReturnBaseAttributes(){
        PrimaryAttribute actual = testMage.getPrimaryAttribute();
        PrimaryAttribute expected = new PrimaryAttribute(1,1,8,5);

        assertTrue(expected.equals(actual));
    }

    @Test
    void getPrimaryAttribute_shouldReturnUpdatedAttributes_whenCharacterIsLeveledUp(){
        testMage.levelUp();

        PrimaryAttribute actual = testMage.getPrimaryAttribute();
        PrimaryAttribute expected = new PrimaryAttribute(2,2,13,8);

        assertTrue(expected.equals(actual));
    }
}