public class VariablesForTesting {

    public static final Weapon wand = new Weapon("TestWeapon", 1, 2,2, WeaponType.WAND);
    public static final Weapon sword = new Weapon("TestWeapon", 1, 1, 1, WeaponType.SWORD);
    public static final Weapon axe = new Weapon("TestWeapon", 1, 1, 1, WeaponType.AXE);
    public static final Weapon hammer = new Weapon("TestWeapon", 1, 2, 2, WeaponType.HAMMER);
    public static final Weapon hammer_level_2 = new Weapon("TestWeapon", 2, 1, 1, WeaponType.HAMMER);
    public static final Weapon dagger = new Weapon("TestWeapon", 1, 2, 2, WeaponType.DAGGER);
    public static final Weapon dagger_level_2 = new Weapon("TestWeapon", 2, 1, 1, WeaponType.DAGGER);
    public static final Weapon staff = new Weapon("TestWeapon", 1, 2, 3, WeaponType.STAFF);
    public static final Weapon staff_level_2 = new Weapon("TestWeapon", 2, 1, 1, WeaponType.STAFF);
    public static final Weapon bow = new Weapon("TestWeapon", 1, 2, 2, WeaponType.BOW);
    public static final Weapon bow_level_2 = new Weapon("TestWeapon", 2, 1, 1, WeaponType.BOW);

    public static final Armor leather = new Armor("TestWeapon", 1, Slots.BODY, ArmorType.LEATHER, new PrimaryAttribute(1,2,1,1));
    public static final Armor mail = new Armor("TestWeapon", 1, Slots.BODY, ArmorType.MAIL, new PrimaryAttribute(1,1,1,1));
    public static final Armor plate = new Armor("TestWeapon", 1, Slots.BODY, ArmorType.PLATE, new PrimaryAttribute(2,2,1,1));
    public static final Armor plate_level_2 = new Armor("TestWeapon", 2, Slots.BODY, ArmorType.PLATE, new PrimaryAttribute(1,1,1,1));
    public static final Armor cloth = new Armor("Cloth", 1, Slots.BODY, ArmorType.CLOTH, new PrimaryAttribute(1,1,1,1));
    public static final Armor cloth_level_2 = new Armor("Cloth", 2, Slots.BODY, ArmorType.CLOTH, new PrimaryAttribute(1,1,1,1));
}
