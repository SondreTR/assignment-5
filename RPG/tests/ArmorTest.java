import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    Armor testArmor = new Armor("testArmor", 1,Slots.BODY,ArmorType.MAIL,
            new PrimaryAttribute(1,2,3,4));

    Armor testArmor_2 = new Armor("testArmor", 1,Slots.BODY,ArmorType.MAIL,
            new PrimaryAttribute(1,2,3,4));

    Armor testArmor_3 = new Armor("testArmor", 1,Slots.BODY,ArmorType.LEATHER,
            new PrimaryAttribute(1,2,3,4));

    @Test
    void getArmorType_shouldReturnGivenArmorType() {
        ArmorType actual = testArmor.getArmorType();
        ArmorType expected = ArmorType.MAIL;

        assertEquals(actual, expected);
    }

    @Test
    void getArmorAttributes() {
        PrimaryAttribute actual = testArmor.getArmorAttributes();
        PrimaryAttribute expected = new PrimaryAttribute(1,2,3,4);

        assertTrue(actual.equals(expected));
    }

    @Test
    void equals_shouldReturnTrue_whenTwoArmorAreEquals(){
        assertTrue(testArmor.equals(testArmor_2));
    }

    @Test
    void equals_shouldReturnFalse_whenTwoArmorAreNotEquals(){
        assertFalse(testArmor.equals(testArmor_3));
    }
}