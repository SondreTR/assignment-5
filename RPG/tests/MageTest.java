import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest extends VariablesForTesting{

    Mage testMage = new Mage("testMage");

    @Test
    void levelUp_shouldLevelUpMageWithTheRightIncreaseInLevel() {
        testMage.levelUp();

        int actual = testMage.getLevel();
        int expected = 2;

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_shouldLevelUpMageWithTheRightIncreaseInAttributes() {
        testMage.levelUp();

        PrimaryAttribute actual = testMage.getPrimaryAttribute();
        PrimaryAttribute expected = new PrimaryAttribute(2,2,13,8);

        assertTrue(actual.equals(expected));
    }

    @Test
    void getClassName_shouldReturnStringMage() {
        String actual = testMage.getClassName();
        String expected = "Mage";

        assertEquals(expected, actual);
    }

    @Test
    void canUseWeapon_shouldReturnTrue_whenWeaponIsWand() {
        boolean actual = testMage.canUseWeapon(wand);
        assertTrue(actual);
    }

    @Test
    void canUseWeapon_shouldReturnTrue_whenWeaponIsStaff() {
        boolean actual = testMage.canUseWeapon(staff);
        assertTrue(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsBow() {
        boolean actual = testMage.canUseWeapon(bow);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsAxe() {
        boolean actual = testMage.canUseWeapon(axe);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsDagger() {
        boolean actual = testMage.canUseWeapon(dagger);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsHammer() {
        boolean actual = testMage.canUseWeapon(hammer);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsSword() {
        boolean actual = testMage.canUseWeapon(sword);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsHigherLevelThanCharacter() {
        boolean actual = testMage.canUseWeapon(staff_level_2);
        assertFalse(actual);
    }

    @Test
    void canUseArmor_shouldReturnTrue_whenArmorIsCloth() {
        boolean actual = testMage.canUseArmor(cloth);

        assertTrue(actual);
    }

    @Test
    void canUseArmor_shouldReturnFalse_whenArmorIsLeather() {
        boolean actual = testMage.canUseArmor(leather);

        assertFalse(actual);
    }

    @Test
    void canUseArmor_shouldReturnFalse_whenArmorIsMail() {
        boolean actual = testMage.canUseArmor(mail);

        assertFalse(actual);
    }

    @Test
    void canUseArmor_shouldReturnFalse_whenArmorIsPlate() {
        boolean actual = testMage.canUseArmor(plate);

        assertFalse(actual);
    }

    @Test
    void canUseArmor_shouldReturnFalse_whenArmorIsHigherLevelThanCharacter() {
        boolean actual = testMage.canUseArmor(cloth_level_2);

        assertFalse(actual);
    }

    @Test
    void getDamage_shouldReturnBaseDamage_whenMageCharacterIsLevelOne() {
        float actual = testMage.getDamage();
        float expected = (1+(8/100f)*1);

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnBaseDamage_whenMageCharacterIsLevelTwo() {
        testMage.levelUp();

        float actual = testMage.getDamage();
        float expected = (1+(13/100f)*1);

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnBaseDamage_whenMageCharacterIsLevelThree() {
        testMage.levelUp();
        testMage.levelUp();

        float actual = testMage.getDamage();
        float expected = (1+(18/100f)*1);

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnDamage_whenMageCharacterIsLevelOneAndEquippedWeapon() {
        testMage.setEquipment(wand);

        float actual = testMage.getDamage();
        float expected = ((1+(8/100f))*(2*2));

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnDamage_whenMageCharacterIsLevelTwoAndEquippedWeapon() {
        testMage.levelUp();
        testMage.setEquipment(wand);

        float actual = testMage.getDamage();
        float expected = ((1+(13/100f))*(2*2));

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnDamage_whenMageCharacterIsLevelOneAndEquippedArmor() {
        testMage.setEquipment(cloth);

        float actual = testMage.getDamage();
        float expected = ((1+(9/100f)));

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnDamage_whenMageCharacterIsLevelTwoAndEquippedArmor() {
        testMage.levelUp();
        testMage.setEquipment(cloth);

        float actual = testMage.getDamage();
        float expected = ((1+(14/100f)));

        assertEquals(expected, actual);
    }
}