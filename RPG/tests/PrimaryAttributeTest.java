import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrimaryAttributeTest {

    PrimaryAttribute testAttributes = new PrimaryAttribute(1,2,3,4);

    @Test
    void getStrength_shouldReturnOne(){
        PrimaryAttribute testAttributes = new PrimaryAttribute(1,0,0,0);

        int actual = testAttributes.getStrength();
        int expected = 1;

        assertEquals(expected, actual);
    }

    @Test
    void getDexterity_shouldReturnOne(){
        PrimaryAttribute testAttributes = new PrimaryAttribute(0,1,0,0);

        int actual = testAttributes.getDexterity();
        int expected = 1;

        assertEquals(expected, actual);
    }

    @Test
    void getIntelligence_shouldReturnOne(){
        PrimaryAttribute testAttributes = new PrimaryAttribute(0,0,1,0);

        int actual = testAttributes.getIntelligence();
        int expected = 1;

        assertEquals(expected, actual);
    }

    @Test
    void getVitality_shouldReturnOne(){
        PrimaryAttribute testAttributes = new PrimaryAttribute(0,0,0,1);

        int actual = testAttributes.getVitality();
        int expected = 1;

        assertEquals(expected, actual);
    }

    @Test
    void equals_shouldReturnTrueIfTwoPrimaryAttributesAreTheSame(){
        boolean actual = testAttributes.equals(new PrimaryAttribute(1,2,3,4));
        assertTrue(actual);
    }

    @Test
    void equals_shouldReturnFalseIfTwoPrimaryAttributesAreNotTheSame(){
        boolean actual = testAttributes.equals(new PrimaryAttribute(1,2,3,3));
        assertFalse(actual);
    }

    @Test
    void addAttribute_shouldReturnSumOfSetAndGivenAttributes(){
        testAttributes.addAttribute(new PrimaryAttribute(1,1,1,1));

        PrimaryAttribute expected = new PrimaryAttribute(2,3,4,5);

        assertTrue(testAttributes.equals(expected));
    }
}