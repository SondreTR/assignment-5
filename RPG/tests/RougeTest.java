import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RougeTest extends VariablesForTesting {

    Rouge testRouge= new Rouge("testRouge");

    @Test
    void levelUp_shouldLevelUpRougeWithTheRightIncreaseInLevel() {
        testRouge.levelUp();

        int actual = testRouge.getLevel();
        int expected = 2;

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_shouldLevelUpRougeWithTheRightIncreaseInAttributes() {
        testRouge.levelUp();

        PrimaryAttribute actual = testRouge.getPrimaryAttribute();
        PrimaryAttribute expected = new PrimaryAttribute(3,10,2,11);

        assertTrue(actual.equals(expected));
    }

    @Test
    void getClassName_shouldReturnStringRouge() {
        String actual = testRouge.getClassName();
        String expected = "Rouge";

        assertEquals(expected, actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsWand() {
        boolean actual = testRouge.canUseWeapon(wand);

        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsStaff() {
        boolean actual = testRouge.canUseWeapon(staff);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsBow() {
        boolean actual = testRouge.canUseWeapon(bow);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsAxe() {
        boolean actual = testRouge.canUseWeapon(axe);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnTrue_whenWeaponIsDagger() {
        boolean actual = testRouge.canUseWeapon(dagger);
        assertTrue(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsHammer() {
        boolean actual = testRouge.canUseWeapon(hammer);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnTrue_whenWeaponIsSword() {
        boolean actual = testRouge.canUseWeapon(sword);
        assertTrue(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsHigherLevelThanCharacter() {
        boolean actual = testRouge.canUseWeapon(dagger_level_2);
        assertFalse(actual);
    }

    @Test
    void canUseArmor_shouldReturnFalse_whenArmorIsCloth() {
        boolean actual = testRouge.canUseArmor(cloth);
        assertEquals(false, actual);
    }

    @Test
    void canUseArmor_shouldReturnTrue_whenArmorIsLeather() {
        boolean actual = testRouge.canUseArmor(leather);
        assertTrue(actual);
    }

    @Test
    void canUseArmor_shouldReturnTrue_whenArmorIsMail() {
        boolean actual = testRouge.canUseArmor(mail);
        assertTrue(actual);
    }

    @Test
    void canUseArmor_shouldReturnFalse_whenArmorIsPlate() {
        boolean actual = testRouge.canUseArmor(plate);
        assertFalse(actual);
    }

    @Test
    void canUseArmor_shouldReturnFalse_whenArmorIsHigherLevelThanCharacter() {
        boolean actual = testRouge.canUseArmor(plate_level_2);
        assertFalse(actual);
    }

    @Test
    void getDamage_shouldReturnBaseDamage_whenRougeCharacterIsLevelOne() {
        float actual = testRouge.getDamage();
        float expected = (1+(6/100f)*1);

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnBaseDamage_whenRougeCharacterIsLevelTwo() {
        testRouge.levelUp();

        float actual = testRouge.getDamage();
        float expected = (1+(10/100f)*1);

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnBaseDamage_whenRougeCharacterIsLevelThree() {
        testRouge.levelUp();
        testRouge.levelUp();

        float actual = testRouge.getDamage();
        float expected = (1+(14/100f)*1);

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnDamage_whenRougeCharacterIsLevelOneAndEquippedWeapon() {
        testRouge.setEquipment(dagger);
        float actual = testRouge.getDamage();
        float expected = ((1+(6/100f))*(2*2));
        assertEquals(expected, actual);
    }

    @Test
    void getDaRouge_shouldReturnDamage_whenRougeCharacterIsLevelTwoAndEquippedWeapon() {
        testRouge.levelUp();
        testRouge.setEquipment(dagger);

        float actual = testRouge.getDamage();
        float expected = ((1+(10/100f))*(2*2));

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnDamage_whenRougeCharacterIsLevelOneAndEquippedArmor() {
        testRouge.setEquipment(leather);

        float actual = testRouge.getDamage();
        float expected = ((1+(8/100f)));

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnDamage_whenRougeCharacterIsLevelTwoAndEquippedArmor() {
        testRouge.levelUp();
        testRouge.setEquipment(leather);

        float actual = testRouge.getDamage();
        float expected = ((1+(12/100f)));

        assertEquals(expected, actual);
    }
}