import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest extends VariablesForTesting {

    Ranger testRanger= new Ranger("testRanger");

    @Test
    void levelUp_shouldLevelUpRangerWithTheRightIncreaseInLevel() {
        testRanger.levelUp();

        int actual = testRanger.getLevel();
        int expected = 2;

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_shouldLevelUpRangerWithTheRightIncreaseInAttributes() {
        testRanger.levelUp();

        PrimaryAttribute actual = testRanger.getPrimaryAttribute();
        PrimaryAttribute expected = new PrimaryAttribute(2,12,2,10);

        assertTrue(actual.equals(expected));
    }

    @Test
    void getClassName_shouldReturnStringRanger() {
        String actual = testRanger.getClassName();
        String expected = "Ranger";

        assertEquals(expected, actual);
    }


    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsWand() {
        boolean actual = testRanger.canUseWeapon(wand);

        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsStaff() {
        boolean actual = testRanger.canUseWeapon(staff);

        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnTrue_whenWeaponIsBow() {
        boolean actual = testRanger.canUseWeapon(bow);
        assertTrue(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsAxe() {
        boolean actual = testRanger.canUseWeapon(axe);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsDagger() {
        boolean actual = testRanger.canUseWeapon(dagger);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsHammer() {
        boolean actual = testRanger.canUseWeapon(hammer);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsSword() {
        boolean actual = testRanger.canUseWeapon(sword);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsHigherLevelThanCharacter() {
        boolean actual = testRanger.canUseWeapon(bow_level_2);
        assertFalse(actual);
    }

    @Test
    void canUseArmor_shouldReturnFalse_whenArmorIsCloth() {
        boolean actual = testRanger.canUseArmor(cloth);
        assertFalse(actual);
    }

    @Test
    void canUseArmor_shouldReturnTrue_whenArmorIsLeather() {
        boolean actual = testRanger.canUseArmor(leather);
        assertTrue(actual);
    }

    @Test
    void canUseArmor_shouldReturnTrue_whenArmorIsMail() {
        boolean actual = testRanger.canUseArmor(mail);
        assertTrue(actual);
    }

    @Test
    void canUseArmor_shouldReturnFalse_whenArmorIsPlate() {
        boolean actual = testRanger.canUseArmor(plate);
        assertFalse(actual);
    }

    @Test
    void canUseArmor_shouldReturnFalse_whenArmorIsHigherLevelThanCharacter() {
        boolean actual = testRanger.canUseArmor(plate_level_2);
        assertFalse(actual);
    }

    @Test
    void getDamage_shouldReturnBaseDamage_whenRangerCharacterIsLevelOne() {
        float actual = testRanger.getDamage();
        float expected = (1+(7/100f)*1);

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnBaseDamage_whenRangerCharacterIsLevelTwo() {
        testRanger.levelUp();

        float actual = testRanger.getDamage();
        float expected = (1+(12/100f)*1);

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnBaseDamage_whenRangerCharacterIsLevelThree() {
        testRanger.levelUp();
        testRanger.levelUp();

        float actual = testRanger.getDamage();
        float expected = (1+(17/100f)*1);

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnDamage_whenRangerCharacterIsLevelOneAndEquippedWeapon() {
        testRanger.setEquipment(bow);

        float actual = testRanger.getDamage();
        float expected = ((1+(7/100f))*(2*2));

        assertEquals(expected, actual);
    }

    @Test
    void getDaRanger_shouldReturnDamage_whenRangerCharacterIsLevelTwoAndEquippedWeapon() {
        testRanger.levelUp();
        testRanger.setEquipment(bow);

        float actual = testRanger.getDamage();
        float expected = ((1+(12/100f))*(2*2));

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnDamage_whenRangerCharacterIsLevelOneAndEquippedArmor() {
        testRanger.setEquipment(mail);

        float actual = testRanger.getDamage();
        float expected = ((1+(8/100f)));

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnDamage_whenRangerCharacterIsLevelTwoAndEquippedArmor() {
        testRanger.levelUp();
        testRanger.setEquipment(mail);

        float actual = testRanger.getDamage();
        float expected = ((1+(13/100f)));

        assertEquals(expected, actual);
    }

}