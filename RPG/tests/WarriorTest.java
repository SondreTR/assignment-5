import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest extends VariablesForTesting {

    Warrior testWarrior= new Warrior("testWarrior");

    @Test
    void levelUp_shouldLevelUpWarriorWithTheRightIncreaseInLevel() {
        testWarrior.levelUp();

        int actual = testWarrior.getLevel();
        int expected = 2;

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_shouldLevelUpWarriorWithTheRightIncreaseInAttributes() {
        testWarrior.levelUp();

        PrimaryAttribute actual = testWarrior.getPrimaryAttribute();
        PrimaryAttribute expected = new PrimaryAttribute(8,4,2,15);

        assertTrue(actual.equals(expected));
    }

    @Test
    void getClassName_shouldReturnStringWarrior() {
        String actual = testWarrior.getClassName();
        String expected = "Warrior";

        assertEquals(expected, actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsWand() {
        boolean actual = testWarrior.canUseWeapon(wand);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsStaff() {
        boolean actual = testWarrior.canUseWeapon(staff);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsBow() {
        boolean actual = testWarrior.canUseWeapon(bow);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnTrue_whenWeaponIsAxe() {
        boolean actual = testWarrior.canUseWeapon(axe);
        assertTrue(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsDagger() {
        boolean actual = testWarrior.canUseWeapon(dagger);
        assertFalse(actual);
    }

    @Test
    void canUseWeapon_shouldReturnTrue_whenWeaponIsHammer() {
        boolean actual = testWarrior.canUseWeapon(hammer);
        assertTrue(actual);
    }

    @Test
    void canUseWeapon_shouldReturnTrue_whenWeaponIsSword() {
        boolean actual = testWarrior.canUseWeapon(sword);
        assertTrue(actual);
    }

    @Test
    void canUseWeapon_shouldReturnFalse_whenWeaponIsHigherLevelThanCharacter() {
        boolean actual = testWarrior.canUseWeapon(hammer_level_2);
        assertFalse(actual);
    }

    @Test
    void canUseArmor_shouldReturnFalse_whenArmorIsCloth() {
        boolean actual = testWarrior.canUseArmor(cloth);
        assertFalse(actual);
    }

    @Test
    void canUseArmor_shouldReturnFalse_whenArmorIsLeather() {
        boolean actual = testWarrior.canUseArmor(leather);
        assertFalse(actual);
    }

    @Test
    void canUseArmor_shouldReturnTrue_whenArmorIsMail() {
        boolean actual = testWarrior.canUseArmor(mail);
        assertTrue(actual);
    }

    @Test
    void canUseArmor_shouldReturnTrue_whenArmorIsPlate() {
        boolean actual = testWarrior.canUseArmor(plate);
        assertTrue(actual);
    }

    @Test
    void canUseArmor_shouldReturnFalse_whenArmorIsHigherLevelThanCharacter() {
        boolean actual = testWarrior.canUseArmor(plate_level_2);
        assertFalse(actual);
    }

    @Test
    void getDamage_shouldReturnBaseDamage_whenWarriorCharacterIsLevelOne() {
        float actual = testWarrior.getDamage();
        float expected = (1+(5/100f)*1);

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnBaseDamage_whenWarriorCharacterIsLevelTwo() {
        testWarrior.levelUp();

        float actual = testWarrior.getDamage();
        float expected = (1+(8/100f)*1);

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnBaseDamage_whenWarriorCharacterIsLevelThree() {
        testWarrior.levelUp();
        testWarrior.levelUp();

        float actual = testWarrior.getDamage();
        float expected = (1+(11/100f)*1);

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnDamage_whenWarriorCharacterIsLevelOneAndEquippedWeapon() {
        testWarrior.setEquipment(hammer);

        float actual = testWarrior.getDamage();
        float expected = ((1+(5/100f))*(2*2));

        assertEquals(expected, actual);
    }

    @Test
    void getDaWarrior_shouldReturnDamage_whenWarriorCharacterIsLevelTwoAndEquippedWeapon() {
        testWarrior.levelUp();
        testWarrior.setEquipment(hammer);

        float actual = testWarrior.getDamage();
        float expected = ((1+(8/100f))*(2*2));

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnDamage_whenWarriorCharacterIsLevelOneAndEquippedArmor() {
        testWarrior.setEquipment(plate);

        float actual = testWarrior.getDamage();
        float expected = ((1+(7/100f)));

        assertEquals(expected, actual);
    }

    @Test
    void getDamage_shouldReturnDamage_whenWarriorCharacterIsLevelTwoAndEquippedArmor() {
        testWarrior.levelUp();
        testWarrior.setEquipment(plate);

        float actual = testWarrior.getDamage();
        float expected = ((1+(10/100f)));

        assertEquals(expected, actual);
    }
}