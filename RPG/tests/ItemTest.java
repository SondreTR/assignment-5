import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    Weapon testWeapon = new Weapon("testWeapon", 1, 2, 3, WeaponType.AXE);
    Mage testMage = new Mage("testMage");


    @Test
    void levelCheck_shouldReturnTrueIfCharacterHasSameLevelOrMoreAsItem() {
        boolean actual = testWeapon.levelCheck(testMage);

        assertTrue(actual);
    }

    @Test
    void levelCheck_shouldReturnFalseIfCharacterHasLowerLevelThanItem() {
        Weapon testWeapon = new Weapon("testWeapon", 2, 2, 3, WeaponType.AXE);
        boolean actual = testWeapon.levelCheck(testMage);

        assertFalse(actual);
    }

    @Test
    void getItemName_shouldReturnItemName() {
        String actual = testWeapon.getItemName();
        String expected = "testWeapon";

        assertEquals(expected, actual);
    }

    @Test
    void getSlots_shouldReturnGivenSlot() {
        Slots actual = testWeapon.getSlot();
        Slots expected = Slots.WEAPON;

        assertEquals(expected, actual);
    }
}