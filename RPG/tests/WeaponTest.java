import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {

    Weapon testWeapon = new Weapon("testWeapon", 1,3,2, WeaponType.AXE);
    Weapon testWeapon_2 = new Weapon("testWeapon", 1,3,2, WeaponType.AXE);
    Weapon testWeapon_3 = new Weapon("testWeapon", 1,3,2, WeaponType.BOW);

    @Test
    void getDPS_shouldReturnDamageTimesSpeed() {
        int actual = testWeapon.getDPS();
        int expected = 2*3;

        assertEquals(expected, actual);
    }

    @Test
    void getWeaponType_shouldReturnGivenWeaponType() {
        WeaponType actual = testWeapon.getWeaponType();
        WeaponType expected = WeaponType.AXE;

        assertEquals(actual, expected);
    }

    @Test
    void equals_shouldReturnTrue_whenTwoWeaponsAreEquals(){
        assertTrue(testWeapon.equals(testWeapon_2));
    }

    @Test
    void equals_shouldReturnFalse_whenTwoWeaponsAreNotEquals(){
        assertFalse(testWeapon.equals(testWeapon_3));
    }
}